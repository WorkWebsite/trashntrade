using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TrashNTrade.Models;

namespace TrashNTrade.DataContext
{
    public class AppDataContext : DbContext
    {
        public AppDataContext(DbContextOptions<AppDataContext> options) : base(options)
        {
            
        }
        
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Waste> Wastes { get; set; }
        public DbSet<CreditTransfer> CreditTransfers { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<MarketPrice> MarketPrices { get; set; }
        public DbSet<Content> Contents { get; set; }
        public DbSet<Location> Locations { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder
                                                            builder)
                                                            {
                                                            builder.Entity<User>()
                                                                .HasIndex(u => u.Tel)
                                                                .IsUnique();
                                                                    }
        

    }
}