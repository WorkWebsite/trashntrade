import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


 const state = {
    nav: 1

  }

 const getters = {
    getNav: state => state.nav 
  }

  const mutations = {
    changeNav(state) {
      state.nav = 2
    }
  }

 const actions = {

  }


export default {
    state,
    mutations,
    getters,
    actions
}