import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    navState: 1
}

const mutations = {
    ChangeNav (state, payload) {
        state.navState = payload
    }
}

const getters = {
    getNavLoged: state => state.navState
}

const actions = {
    changeNav: ({ commit }, amount) => commit('ChangeNav', amount)
}


export default {
    state,
    mutations,
    getters,
    actions,
}