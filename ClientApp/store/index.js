import Vue from 'vue'
import Vuex from 'vuex'
import NavMenu from './modules/NavMenu'
import NavLoged from './modules/NavLoged'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    NavMenu,
    NavLoged
  }
})