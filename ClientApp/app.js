import Vue from 'vue'
import axios from 'axios'
import router from './router/index'
import store from './store/index'
import { sync } from 'vuex-router-sync'
import App from 'views/app-root'
import { FontAwesomeIcon } from './icons'
import firebase from 'firebase'
import firebaseui from 'firebaseui'
import {config} from './helpers/firebaseConfig'
import Sweetalert from 'sweetalert'


import footerBottom from 'components/footer-bottom'
import home from 'views/home-page'
import login from 'components/login'
import NavMenu from 'components/nav-menu'
import register from 'components/register'
import logotnt from 'components/logo'
import Slides from 'components/Slides'
import NavLoged from 'components/nav-loged'
import Test from 'components/Test'
import Loading from 'components/loading'



/*AOS*/

import AOS from 'aos'
import 'aos/dist/aos.css';

/*Swiper*/

import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'

Vue.use(VueAwesomeSwiper, /* { default global options } */)

/*Vmodal*/

import vmodal from 'vue-js-modal'
    Vue.use(vmodal)

/*VeeValidata*/
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);

/*Vuetify*/

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

/*material design icons iconfont*/

// Registration of global components
Vue.component('icon', FontAwesomeIcon)

Vue.prototype.$http = axios

sync(store, router)

const app = new Vue({
  store,
  router,
  ...App,
  components:{
    footerBottom,
    home,
    login,
    NavMenu,
    register,
    logotnt,
    Slides,
    NavLoged,
    Test,
    Loading,
  },

  created() {
    AOS.init();

    firebase.initializeApp(config);
    firebase.auth().onAuthStateChanged((user) => {
        if(user){
          this.$router.push('/success')
        }else{
          this.$router.push('/')
        }
    });
  }
  
})


export {
  app,
  router,
  store,
  VueAwesomeSwiper
}
