import HomePage from 'views/home-page'
import About from 'views/about'
import Login from 'components/login'
import Register from 'components/register'
import MainFeature from 'views/main-feature'
import MarketPrice from 'components/market-price'
import Admin from 'views/admin'



 export const routes = [
 { name: 'home', path: '/', component: HomePage, display: 'Home' },
 { name: 'about', path: '/about', component: About, display: 'About' },
 { name: 'login', path: '/login', component: Login, display: 'Login' },
 { name: 'register', path: '/register', component: Register, display: 'Register' },
 { name: 'admin', path: '/admin', component: Admin, display: 'Admin' },
 { name: 'mainFeature', path: '/success', component: MainFeature, display: 'MainFeature'},
 { name: 'marketprice', path: '/marketprice', component: MarketPrice, display: 'MarketPrice'}
]
