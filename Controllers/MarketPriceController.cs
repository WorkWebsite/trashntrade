using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Oracle.ManagedDataAccess.Client;
using TrashNTrade.Models;

namespace TrashNTrade.Controllers
{
    public class MarketPriceController
    {
        // [HttpPost]
        //     public object CreateMarketPrice([FromBody]MarketPrice marketprice) 
        //     {
                
        //         string result = "";
        //         try
        //         {
        //             MySqlConnection mySqlCon = new MySqlConnection("server=localhost;database=trashntrade;user=root;pwd=;sslmode=none;");
        //             mySqlCon.Open();
        //             string MysqlQuery = "INSERT INTO market_price(Type,Name,Value,Date) VALUES (@tp,@nm,@vl,@dt)";
        //             // string MysqlQuery = "INSERT INTO market_price(Type,Name,Value,Date) VALUES ('" + marketprice.Type + "','" + marketprice.Name + "','" + marketprice.Value + "','" + marketprice.Date.ToString() + "')";
        //             MySqlCommand mySqlCom = new MySqlCommand(MysqlQuery);
        //             mySqlCom.Connection = mySqlCon;

        //             mySqlCom.Parameters.AddWithValue("@tp",marketprice.Type);
        //             mySqlCom.Parameters.AddWithValue("@nm",marketprice.Name);
        //             mySqlCom.Parameters.AddWithValue("@vl",marketprice.Value);
        //             mySqlCom.Parameters.AddWithValue("@dt",marketprice.Date);
                    
        //             MySqlDataReader mySqlReader = mySqlCom.ExecuteReader();                    
        //             result = "success!";
        //         }
        //         catch(Exception fail){
        //             result = "fail" + fail;
        //         }
        //        return result;
        //     }

            [HttpPost]
            public object InsertMarketPrice([FromBody]List<MarketPrice> marketprice)
            {
                int count = 0;

                MySqlConnection mySqlCon = new MySqlConnection("server=localhost;database=trashntrade;user=root;pwd=;sslmode=none; charset=utf8;");
                mySqlCon.Open();
                string MysqlQuery = "INSERT INTO market_price(Type,Name,Value,Date) VALUES (@tp,@nm,@vl,@dt)";
                MySqlCommand mySqlCom = new MySqlCommand(MysqlQuery);
                mySqlCom.Connection = mySqlCon;
                var dt = DateTime.Now;
                mySqlCom.Parameters.AddWithValue("@tp","");
                mySqlCom.Parameters.AddWithValue("@nm","");
                mySqlCom.Parameters.AddWithValue("@vl",0);
                mySqlCom.Parameters.AddWithValue("@dt",dt);

                foreach (var item in marketprice)
                {
                    mySqlCom.Parameters["@tp"].Value = item.Type;
                    mySqlCom.Parameters["@nm"].Value = item.Name;
                    mySqlCom.Parameters["@vl"].Value = item.Value;
                    mySqlCom.Parameters["@dt"].Value = item.Date;
                    var i = mySqlCom.ExecuteNonQuery();
                    count += i;
                }

                return marketprice;
    
            }

            [HttpPost]
            public object UpdateMarketPrice([FromBody]List<MarketPrice> marketprice)
            {
                int count = 0;

                MySqlConnection mySqlCon = new MySqlConnection("server=localhost;database=trashntrade;user=root;pwd=;sslmode=none; charset=utf8;");
                mySqlCon.Open();
                string MysqlQuery = "UPDATE market_price SET EndDate = @dt WHERE Type = @tp AND Name = @nm";
                MySqlCommand mySqlCom = new MySqlCommand(MysqlQuery);
                mySqlCom.Connection = mySqlCon;
                var dt = DateTime.Now;
                mySqlCom.Parameters.AddWithValue("@tp","");
                mySqlCom.Parameters.AddWithValue("@nm","");
                mySqlCom.Parameters.AddWithValue("@dt",dt);

                foreach (var item in marketprice)
                {
                    mySqlCom.Parameters["@tp"].Value = item.Type;
                    mySqlCom.Parameters["@nm"].Value = item.Name;
                    mySqlCom.Parameters["@dt"].Value = item.EndDate;
                    var i = mySqlCom.ExecuteNonQuery();
                    count += i;
                }

                return marketprice;
    
            }

            [HttpPost]
            public object DeleteMarketPrice([FromBody]List<MarketPrice> marketprice)
            {
                int count = 0;

                MySqlConnection mySqlCon = new MySqlConnection("server=localhost;database=trashntrade;user=root;pwd=;sslmode=none; charset=utf8;");
                mySqlCon.Open();
                string MysqlQuery = "DELETE FROM market_price WHERE Type = @tp AND Name = @nm";
                MySqlCommand mySqlCom = new MySqlCommand(MysqlQuery);
                mySqlCom.Connection = mySqlCon;
                var dt = DateTime.Now;
                mySqlCom.Parameters.AddWithValue("@tp","");
                mySqlCom.Parameters.AddWithValue("@nm","");

                foreach (var item in marketprice)
                {
                    mySqlCom.Parameters["@tp"].Value = item.Type;
                    mySqlCom.Parameters["@nm"].Value = item.Name;
                    var i = mySqlCom.ExecuteNonQuery();
                    count += i;
                }

                return marketprice;
    
            }

            [HttpGet]
            public List<MarketPrice> SelectMarketPriceAll()
            {

                MySqlConnection mySqlCon = new MySqlConnection("server=localhost;database=trashntrade;user=root;pwd=;sslmode=none; charset=utf8;");
                mySqlCon.Open();
                string MysqlQuery = "SELECT * FROM market_price WHERE Mid IN (SELECT MAX(Mid) FROM market_price GROUP BY Name) ORDER BY Mid DESC";
                MySqlCommand mySqlCom = new MySqlCommand(MysqlQuery);
                mySqlCom.Connection = mySqlCon;

                List<MarketPrice> mp = new List<MarketPrice>();

                var r = mySqlCom.ExecuteReader();

                while (r.Read())
                {
                    mp.Add(new MarketPrice{
                        Mid = r.GetInt32("Mid"),
                        Type = r.GetString("Type"),
                        Name = r.GetString("Name"),
                        Value = r.GetInt32("Value"),
                        Date = r.GetDateTime("Date"),
                    });
                }

                return mp;
            } 

            [HttpGet("[controller]/[action]/{type}")]
            public List<MarketPrice> SelectMarketPriceType(string type)
            {

                MySqlConnection mySqlCon = new MySqlConnection("server=localhost;database=trashntrade;user=root;pwd=;sslmode=none; charset=utf8;");
                mySqlCon.Open();
                string MysqlQuery = "SELECT * FROM market_price WHERE Mid IN (SELECT MAX(Mid) FROM market_price GROUP BY Name) AND Type = '" + type +"' ORDER BY Mid DESC";
                MySqlCommand mySqlCom = new MySqlCommand(MysqlQuery);
                mySqlCom.Connection = mySqlCon;

                List<MarketPrice> mp = new List<MarketPrice>();

                var r = mySqlCom.ExecuteReader();

                while (r.Read())
                {
                    mp.Add(new MarketPrice{
                        Mid = r.GetInt32("Mid"),
                        Type = r.GetString("Type"),
                        Name = r.GetString("Name"),
                        Value = r.GetInt32("Value"),
                        Date = r.GetDateTime("Date"),
                    });
                }

                return mp;
            } 
    }
}