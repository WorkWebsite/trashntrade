using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TrashNTrade.DataContext;
using TrashNTrade.Models;
using System.Collections.Generic;

namespace TrashNTrade.Controllers
{
    public class TransactionController : Controller
    {
        private readonly AppDataContext context;

        public TransactionController(AppDataContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        // get

        [HttpGet("[controller]/[action]/{user}")]
        public object GetTransaction(string user)
        {
            var transaction = (from t in context.Transactions
            join l in context.Locations on t.LocationID equals l.LId
            where t.SellerId.Contains(user) || t.BuyerId.Contains(user)
            select new {
                trans = t,
                address = l.AddressString,
                nameStore = l.Name,
                title = l.Title,
                imgStore = l.ProfileImg,
                waste = from w in context.Wastes
                where w.Transactionid == t.TId
                select new {
                    waste = w
                }
            }).ToList();

            return transaction;
        }

        public Transaction DeletePost([FromBody]Transaction transaction)
        {
            var tran = (from t in context.Transactions
            join l in context.Locations on t.LocationID equals l.LId
            join w in context.Wastes on t.TId equals w.Transactionid
            where t.TId == transaction.TId && t.LocationID == l.LId && w.Transactionid == transaction.TId
            select new {
                tran = t,locate = l,waste = w
            }).ToList();

            foreach(var t in tran)
            {
                context.Transactions.Remove(t.tran);
                context.Locations.Remove(t.locate);
                context.Wastes.Remove(t.waste);
            }
            context.SaveChanges();
            return transaction;

        }

        public Transaction DeleteStore([FromBody]Transaction transaction)
        {
            var tran = (from t in context.Transactions
            join l in context.Locations on t.LocationID equals l.LId
            where t.TId == transaction.TId && t.LocationID == l.LId
            select new {
                tran = t, locate = l
            }).ToList();

            foreach(var t in tran)
            {
                context.Transactions.Remove(t.tran);
                context.Locations.Remove(t.locate);
            }
            context.SaveChanges();
            return transaction;

        }

        [HttpGet]
        public object SelectBuyerTransaction()
        {
            var user = (from u in context.Users
            where u.Type == "Buyer"
            select new {
                user = u.UId,
                tran = from t in context.Transactions where u.UId == t.BuyerId
                group t by t.BuyerId into tq
                select new {
                    tran = tq.Count(),
                }
            }).ToList();

            return user;

        }

        

        public List<Waste> GetWaste()
        {
            var waste = context.Wastes.ToList();
            return waste;
        }


        // Insert

        [HttpPost]
        public Location InsertLocation([FromBody]Location location)
        {
            context.Locations.Add(location);
            context.SaveChanges();
                
            return location;
        }

        [HttpPost]
        public List<Waste> InsertGarbage([FromBody]List<Waste> waste)
        {
            foreach (var item in waste)
            {
                context.Wastes.Add(item);
                context.SaveChanges();
                
            }
            return waste;
        }

        [HttpPost]
        public Transaction InsertTransaction([FromBody]Transaction transaction)
        {
            context.Transactions.Add(transaction);
            context.SaveChanges();

            return transaction;
        }

        [HttpPost]
        public Location InsertStore([FromBody]Location location)
        {
            context.Locations.Add(location);
            context.SaveChanges();
                
            return location;
        }

        // Update

        [HttpPost]
        public Location UpdateStore([FromBody]Location location) {

            var locate = context.Locations.Find(location.LId);
            if(locate != null) {
                locate.AddressString = location.AddressString;
                locate.Name = location.Name;
                locate.Title = location.Title;
                locate.ProfileImg = location.ProfileImg;
            }
            context.Locations.Update(locate);
            context.SaveChanges();
            return locate;
        }

        [HttpPost]
        public Transaction UpdateStatusSelling([FromBody]Transaction transaction) {

            var tran = context.Transactions.Find(transaction.TId);
            if(tran != null) {
                tran.Status = transaction.Status;
                tran.BuyerId = transaction.BuyerId;
            }
            context.Transactions.Update(tran);
            context.SaveChanges();
            return tran;
        }

        [HttpPost]
        public Transaction UpdateStatusConfirm([FromBody]Transaction transaction) {

            var tran = context.Transactions.Find(transaction.TId);
            if(tran != null) {
                tran.Status = transaction.Status;
            }
            context.Transactions.Update(tran);
            context.SaveChanges();
            return tran;
        }

        [HttpPost]
        public Transaction UpdateStatusSold([FromBody]Transaction transaction) {

            var tran = context.Transactions.Find(transaction.TId);
            if(tran != null) {
                tran.Status = transaction.Status;
            }
            context.Transactions.Update(tran);
            context.SaveChanges();
            return tran;
        }

    }
}
