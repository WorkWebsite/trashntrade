using Microsoft.AspNetCore.Mvc;
using Oracle.ManagedDataAccess.Client;
using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using TrashNTrade.DataContext;
using TrashNTrade.Models;
using System.Collections.Generic;


namespace TrashNTrade.Controllers
{
    public class AdminController
    {
        
         [HttpPost]
            public object CreateReport([FromBody]Report report) 
            {
                
                string result = "";
                try
                {
                    OracleConnection oraclecon = new OracleConnection("Pooling=false;User Id=trashntradeoracle;Password=1234;Data Source=localhost:1521/xe;");
                    oraclecon.Open();
                    string OracleQuery = "INSERT INTO REPORT(Name,Email,Content,Datetime,Status) VALUES ('"+report.Name+"','"+report.Email+"','"+report.Content+"',TO_DATE(SYSDATE,'dd-mm-yy HH24:MI:SS'), 1)";
                    OracleCommand oracleCommand = new OracleCommand(OracleQuery);
                    oracleCommand.Connection = oraclecon;
                    OracleDataReader oracleReader1 = oracleCommand.ExecuteReader();
                    result = "success!";
                }
                catch(Exception fail){
                    result = "fail " + fail;
                }
               return result;
            }

    //         [HttpGet]
    //         public List<Report> SelectReport() 
    //         {
                
                
    //                 OracleConnection oraclecon = new OracleConnection("Pooling=false;User Id=trashntradeoracle;Password=1234;Data Source=localhost:1521/xe;");
    //                 oraclecon.Open();
    //                 string OracleQuery = "SELECT RID,NAME,EMAIL,CONTENT,DATETIME,STATUS FROM REPORT";
    //                 OracleCommand oracleCommand = new OracleCommand(OracleQuery);
    //                 oracleCommand.Connection = oraclecon;
                    
    //                 List<Report> reports = new List<Report>();
    //                 OracleDataReader oracleReader1 = oracleCommand.ExecuteReader();
    //                 while  (oracleReader1.Read())
    //                 {
    //                     reports.Add(new Report
    //                     {
    //                         Rid = oracleReader1.GetInt32(oracleReader1.GetOrdinal("Rid")),
    //                         Name = oracleReader1["Name"].ToString(),
    //                         Email = oracleReader1["Email"].ToString(),
    //                         Content = oracleReader1["Content"].ToString(),
    //                         Datetime = oracleReader1.GetDateTime(4),
    //                         Status = oracleReader1.GetInt32(oracleReader1.GetOrdinal("Status"))
    //                     });

    //                 }       
    //                 return reports;
                            
    // }
        [HttpGet("admin/deletestatusreport/{id:int}")]

            public bool DeleteStatusReport(int id) 
            {
                
                
                    OracleConnection oraclecon = new OracleConnection("Pooling=false;User Id=trashntradeoracle;Password=1234;Data Source=localhost:1521/xe;");
                    oraclecon.Open();
                    string OracleQuery = "DELETE FROM REPORT WHERE RID = :id";
                    OracleCommand oracleCommand = new OracleCommand(OracleQuery);
                    oracleCommand.Connection = oraclecon;
                    oracleCommand.Parameters.Add("id",id);
                    var x = oracleCommand.ExecuteNonQuery();

                    if(x > 0){
                        return true;
                    }

                    return false;
                            
    }
    [HttpPatch("admin/changestatus/{id:int}")]
    public bool ChangeStatus(int id)
    {
        OracleConnection oraclecon = new OracleConnection("Pooling=false;User Id=trashntradeoracle;Password=1234;Data Source=localhost:1521/xe;");
        oraclecon.Open();
        string OracleQuery = "UPDATE REPORT SET STATUS = 0 WHERE RID = :id";
        OracleCommand oracleCom = new OracleCommand(OracleQuery, oraclecon);

        oracleCom.Parameters.Add("id",id);
        var y = oracleCom.ExecuteNonQuery();

        return y == 1 ? true: false;

    }
}
}