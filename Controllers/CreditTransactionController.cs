using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TrashNTrade.DataContext;
using TrashNTrade.Models;
using System.Collections.Generic;

namespace TrashNTrade.Controllers
{
    public class CreditTransactionController : Controller
    {
        private readonly AppDataContext context;

        public CreditTransactionController(AppDataContext context)
            {
                 this.context = context;
            }

        [HttpGet]
        public List<User> GetReceiver()
        {
            var usr = context.Users.ToList();
            return usr;
        }

        [HttpGet("Credittransaction/gethistory/{user}")]
        public List<CreditTransfer> GetHistory(string user)
        {
            var cre = context.CreditTransfers.Where(x=>x.SenderId == user).ToList();

            return cre;
        }

        [HttpPost]
            public User sendCredit([FromBody]User user) {

                var usr = context.Users.Find(user.UId);
                if(usr != null) {
                    usr.Wallet += user.Wallet;
                }
                context.Users.Update(usr);
                context.SaveChanges();
                return usr;
        }

            [HttpPost]
            public User updateCredit([FromBody]User user) {

                var usr = context.Users.Find(user.UId);
                if(usr != null) {
                    usr.Wallet -= user.Wallet;
                }
                context.Users.Update(usr);
                context.SaveChanges();
                return usr;
            }

            [HttpPost]
            public CreditTransfer CreateCredittransaction([FromBody]CreditTransfer creditTransfer) {

                context.CreditTransfers.Add(creditTransfer);
                context.SaveChanges();

                return creditTransfer;
            }
    }
}