using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TrashNTrade.DataContext;
using TrashNTrade.Models;
using System.Collections.Generic;

namespace TrashNTrade.Controllers
{
    public class LocationController : Controller
    {
        private readonly AppDataContext context;

        public LocationController(AppDataContext context)
        {
            this.context = context;
        }
        
        

        [HttpGet]

        public object GetLocation()
        {
            
            var location = (from o in context.Locations
                            join u in context.Users on o.OwnerLocation equals u.UId
                            join s in context.Transactions on  o.LId equals s.LocationID
                            where s.Status != "Sold" 
                            select new {
                                Name = u.Name,
                                Tel = u.Tel,
                                Profile = u.ProfileImg,
                                Address = o.AddressString,
                                Lat = o.Latitude,
                                Lng = o.Longtitude,
                                Type = o.type,
                                NameStore = o.Name,
                                TitleStore = o.Title,
                                ImgStore = o.ProfileImg,
                                Owner = o.OwnerLocation,
                                trans = from t in context.Transactions
                                where t.LocationID == o.LId
                                select new {
                                    t_id = t.TId,
                                    t_status = t.Status,
                                    t_date = t.Date
                                },
                                AllTrans = from Al in context.Locations
                                where Al.OwnerLocation == o.OwnerLocation
                                select new {
                                    allLoation = Al.AddressString
                                },
                                waste = from w in context.Wastes
                                join t in context.Transactions on w.Transactionid equals t.TId
                                where t.LocationID == o.LId && t.TId == w.Transactionid
                                select new {
                                    t_id = t.TId,
                                    t_status = t.Status,
                                    t_total = t.TotalValue,
                                    t_date = t.Date,                                   
                                    w_name = w.Name,
                                    w_img = w.Img,
                                    w_type = w.Type,
                                    w_amount = w.Amount,
                                    w_price = w.Price,
                                    w_total_price = w.TotalPrice
                                }
                                
                            }).ToList();

            return location;
        }

        [HttpPost]
        public Location SaveLocation([FromBody]Location location)
        {
            context.Locations.Add(location);
            context.SaveChanges();

            return location;
        }
    }
}