using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TrashNTrade.DataContext;
using TrashNTrade.Models;
using System.Collections.Generic;

    namespace TrashNTrade.Controllers
    {
        public class UserController : Controller
        {
            private readonly AppDataContext context;
            
            public UserController(AppDataContext context)
            {
                 this.context = context;
            }

            [HttpGet]
            public IActionResult Index()
            {
                return View();
            }

            // Insert

            [HttpPost]
            public User CreateUser([FromBody]User user) {

                context.Users.Add(user);
                context.SaveChanges();

                return user;
            }

            // Update

            [HttpPost]
            public User UpdateProfile([FromBody]User user) {

                var usr = context.Users.Find(user.UId);
                if(usr != null) {
                    usr.Name = user.Name;
                    usr.Tel = user.Tel;
                    usr.ProfileImg = user.ProfileImg;
                }
                context.Users.Update(usr);
                context.SaveChanges();
                return usr;
            }

            // Edit Get

           /*[HttpGet]
            public IActionResult Edit(int id) {
                var usr = context.Users.Find(id);
                return View(usr);
            }

            // Edit Post

            [HttpPost]
            public IActionResult Edit(User model) {
                if (!ModelState.IsValid) return View(model);
                context.Users.Update(model);
                context.SaveChanges();
                return RedirectToAction("index");
            }

            // Delete

            [HttpGet]
            public IActionResult Delete(int id){
                var usr = context.Users.Find(id);
                context.Users.Remove(usr);
                context.SaveChanges();
                return RedirectToAction("index");
            }*/ 

        }
    }
