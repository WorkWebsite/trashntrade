using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TrashNTrade.DataContext;
using TrashNTrade.Models;
using System.Collections.Generic;

    namespace TrashNTrade.Controllers
    {
    public class HomeController : Controller
    {
        private readonly AppDataContext context;
        public HomeController(AppDataContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("home/getuser/{user}")]
        public User GetUser(string user)
        {
            var usr = context.Users.Where(x=>x.UId == user).FirstOrDefault();
            return usr;
        }

        // Insert

        /*[HttpPost]
        public IActionResult Create(Buyer model) {
            if (!ModelState.IsValid) return View(model);
            context.Add(model);
            context.SaveChanges();
            return RedirectToAction("index");
        }

        // Edit Get

        [HttpGet]
        public IActionResult Edit(int id) {
            var usr = context.Buyers.Find(id);
            return View(usr);
        }

        // Edit Post

        [HttpPost]
        public IActionResult Edit(Buyer model) {
            if (!ModelState.IsValid) return View(model);
            context.Buyers.Update(model);
            context.SaveChanges();
            return RedirectToAction("index");
        }

        // Delete

        [HttpGet]
        public IActionResult Delete(int id){
            var usr = context.Buyers.Find(id);
            context.Buyers.Remove(usr);
            context.SaveChanges();
            return RedirectToAction("index");
        }*/

    }
    }
