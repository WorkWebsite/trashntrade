using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrashNTrade.Models
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]      
        public string UId { get; set; }
        public string Name {get; set;}
        [Required]
        public string Email {get; set;}
        public string ProfileImg {get; set;}
        [Column(TypeName = "varchar(10)")]
        public string Tel {get; set;}
        public int Wallet {get; set;}
        [Required]
        public string Type {get; set;}

        public ICollection<CreditTransfer> SenderTransfer { get; set; }
        public ICollection<CreditTransfer> ReceiverTransfer { get; set; }
        public ICollection<Transaction> Buyer { get; set;}
        public ICollection<Transaction> Seller { get; set;}
        public ICollection<Location> Owner { get; set; }
        public ICollection<Waste> OwnerWastes { get; set; }


    }
}