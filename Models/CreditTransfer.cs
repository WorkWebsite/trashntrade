using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrashNTrade.Models
{
    public class CreditTransfer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int CId { get; set; }
        public int Amount { get; set; }

        [ForeignKey("SenderTransfer")]
        public string SenderId { get; set; }
        [ForeignKey("ReceiverTransfer")]
        public string ReceiverId { get; set; }
        
        [Column(TypeName = "varchar(10)")]
        public string Tel { get; set; }

        public DateTime? Date { get; set; }



        [InverseProperty("SenderTransfer")]
        public User SenderTransfer { get; set;}
        [InverseProperty("ReceiverTransfer")]
        public User ReceiverTransfer { get; set;}
        
        
    }
}