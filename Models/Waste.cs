using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrashNTrade.Models
{
    public class Waste
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            [Key]
            public int WId { get; set; }
            public string Name { get; set; }
            public string Img { get; set; }
            public string Type { get; set; }
            public int Amount { get; set; }

            public int Price { get; set; }
            public int TotalPrice { get; set; }

            [ForeignKey("Tid")]
            public int Transactionid { get; set; }
            public DateTime? Date { get; set; }

            public virtual Transaction Tid { get; set; }

    }
}
