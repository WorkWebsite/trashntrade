using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrashNTrade.Models
{
    public class Admin
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
         public int AId { get; set; }
         public string Username { get; set; }
         public string Password { get; set; }
    }
}