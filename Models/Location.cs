using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrashNTrade.Models
{
    public class Location
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int LId { get; set; }
        
        public string AddressString { get; set; }

        [Column(TypeName = "decimal(12, 9)")]
        public decimal Latitude { get; set; }
        [Column(TypeName = "decimal(12, 9)")]
        public decimal Longtitude { get; set; }

        public string type { get; set;}

        public string Name { get; set; }

        public string Title { get; set; }

        public string ProfileImg { get; set; }
        [ForeignKey("Owner")]
        public string OwnerLocation { get; set; }

        
        public virtual User Owner { get; set;}

        public ICollection<Transaction> Locations { get; set; }

    }
}