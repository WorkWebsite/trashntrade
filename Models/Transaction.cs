using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrashNTrade.Models
{
    public class Transaction
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TId { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        
        [ForeignKey("Buyer")]
        public string BuyerId { get; set; }
        
        [ForeignKey("Seller")]
        public string SellerId { get; set; }

        public int TotalValue { get; set; }

        public DateTime? Date { get; set; }

        [ForeignKey("Locations")]
        public int LocationID { get; set; }


        [InverseProperty("Buyer")]
        public User Buyer { get; set;}
        
        [InverseProperty("Seller")]
        public User Seller { get; set;}

        public virtual Location Locations { get; set; }

        
    }
}
