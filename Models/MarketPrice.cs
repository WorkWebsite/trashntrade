using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrashNTrade.Models
{
    public class MarketPrice
    {
        [Key]
        public int Mid { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? Date { get; set; }

        public DateTime? EndDate { get; set; }
    }
}