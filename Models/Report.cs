using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrashNTrade.Models
{
    public class Report
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
         public int Rid { get; set; }
         public string Name { get; set; }
         public string Email { get; set; }
         public string Content { get; set; }
         public DateTime Datetime { get; set; }
         public int Status {get; set; }

    }
}