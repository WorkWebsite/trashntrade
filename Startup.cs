using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TrashNTrade.DataContext;
using TrashNTrade.Models;

namespace TrashNTrade
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<AppDataContext>(OptionsBuilderConfigurationExtensions => OptionsBuilderConfigurationExtensions.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                // Webpack initialization with hot-reload.
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true,
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });

                    /* Select DB to vuejs */

                    routes.MapRoute(
                    name: "getuser",
                    template: "{controller}/{action}/{user?}",
                    defaults: new { controller = "Home", action = "GetUser"});


                    routes.MapRoute(
                    name: "getwaste",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Transaction", action = "GetWaste"});

                    routes.MapRoute(
                    name: "select_buyer_transaction",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Transaction", action = "SelectBuyerTransaction"});


                    /* Insert DB */

                    routes.MapRoute(
                    name: "insert_user",
                    template: "{controller}/{action}",
                    defaults: new { controller = "User", action = "CreateUser"});

                    routes.MapRoute(
                    name: "insert_transaction",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Transaction", action = "InsertTransaction"});

                    routes.MapRoute(
                    name: "insert_location",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Transaction", action = "InsertLocation"});

                    routes.MapRoute(
                    name: "insert_garbage",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Transaction", action = "InsertGarbage"});

                    routes.MapRoute(
                    name: "insert_store",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Transaction", action = "InsertStore"});

                    routes.MapRoute(
                    name: "insert_marketprice",
                    template: "{controller}/{action}",
                    defaults: new { controller = "MarketPrice", action = "InsertMarketPrice"});

                    //

                    /* Update DB */

                    routes.MapRoute(
                    name: "update_user",
                    template: "{controller}/{action}",
                    defaults: new { controller = "User", action = "UpdateProfile"});

                    routes.MapRoute(
                    name: "update_store",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Transaction", action = "UpdateStore"});

                    routes.MapRoute(
                    name: "update_status_selling",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Transaction", action = "UpdateStatusSelling"});

                    routes.MapRoute(
                    name: "update_status_confirm",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Transaction", action = "UpdateStatusConfirm"});

                    routes.MapRoute(
                    name: "update_status_sold",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Transaction", action = "UpdateStatusSold"});

                    routes.MapRoute(
                    name: "update_marketprice",
                    template: "{controller}/{action}",
                    defaults: new { controller = "MarketPrice", action = "UpdateMarketPrice"});

                    /* Delete DB */

                    routes.MapRoute(
                    name: "delete_post",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Transaction", action = "DeletePost"});

                    routes.MapRoute(
                    name: "delete_store",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Transaction", action = "DeleteStore"});

                    routes.MapRoute(
                    name: "delete_marketprice",
                    template: "{controller}/{action}",
                    defaults: new { controller = "MarketPrice", action = "DeleteMarketPrice"});

                    
                    // GetLocations
                    routes.MapRoute(
                        name: "getLocation",
                        template: "{controller}/{action}",
                        defaults: new { controller = "Location", action = "GetLocation"}
                    );

                    //update Credit
                    routes.MapRoute(
                    name: "sendCredit",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Credittransaction", action = "sendCredit"});

                    routes.MapRoute(
                    name: "updateCredit",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Credittransaction", action = "updateCredit"});

                     routes.MapRoute(
                    name: "getReceiver",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Credittransaction", action = "GetReceiver"});

                    routes.MapRoute(
                    name: "createCreditTransaction",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Credittransaction", action = "CreateCredittransaction"});

                    routes.MapRoute(
                    name: "createAdmin",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Admin", action = "CreateReport"});
                    
                    routes.MapRoute(
                    name: "createContent",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Admin", action = "SelectReport"});


            });
        }
    }
}
